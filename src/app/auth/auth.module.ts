import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthRoutingModule } from 'src/app/auth/auth-routing.module';
import { SharedModule } from '../shared/share.module';
import { LoginComponent } from './pages';

@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    FormsModule,
    ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
    SharedModule,
  ],
  declarations: [
    LoginComponent,
  ],
  providers: [],
  exports: []
})

export class AuthModule {

}
