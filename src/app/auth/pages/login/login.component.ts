import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/core/services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticatedUser } from 'src/app/core/services/authenticationservice/authenticated-user';
import { UserType } from 'src/app/core/enum/user-type';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})


export class LoginComponent implements OnInit {
  LoginForm: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private router: Router
  ) {

  }

  ngOnInit() {
    this.LoginForm = this.formBuilder.group({
      usercode: ['test', [Validators.required]],
      password: ['123456', [Validators.required, Validators.minLength(6)]]
    });
  }

  get f() { return this.LoginForm.controls; }
  onLogin() {

    this.submitted = true;

    if (this.LoginForm.valid) {
      this.authenticationService.login(this.LoginForm.value.usercode, this.LoginForm.value.password).subscribe((data: AuthenticatedUser) => {
        if (data.userType == UserType.customer) {
          this.router.navigateByUrl("/customer");
        }
      });
    }
  }

}

