
import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import { Observable } from 'rxjs';
import { AuthenticationService, AlertService } from '../../core/services';
@Injectable({ providedIn: 'root' })

export class AuthGuard implements CanActivate {

  constructor(private router: Router, private auth: AuthenticationService, private alertService: AlertService) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {

    if (this.auth.authenticated) {
      var _userType = parseInt(this.auth.getAuth().userType.toString());

      if (route.data.userTypes.indexOf(_userType) == -1) {
        // userType not authorised so redirect to home page
        this.alertService.warning("You are Unauthorized user for this page.");
        this.auth.logout();

        this.router.navigate(['/']);
        return false;
      }

      // authorised so return true
      return true;
    }
    else {
      this.router.navigate([''], { queryParams: { returnUrl: state.url } });
      return false;
    }
  }
}
