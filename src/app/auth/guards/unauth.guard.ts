
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../../core/services';
@Injectable({ providedIn: 'root' })

export class UnAuthGuard implements CanActivate {

  constructor(private router: Router, private auth: AuthenticationService, ) { }

  canActivate(): boolean {

    if (this.auth.authenticated) {
      var redirectURL: string;
      var auth = this.auth.getAuth();
      if (auth && auth.isCustomer) {
        redirectURL = "/customer";
      }
      this.router.navigateByUrl(redirectURL);
      return false;
    }

    return true;
  }
}
