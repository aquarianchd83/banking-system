import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './not-found/not-found.component';
import { UnAuthGuard } from './auth/guards/unauth.guard';
import { UserType } from './core/enum/user-type';
import { AuthGuard } from './auth/guards/auth.guard';

const routes: Routes = [
    { path: '', redirectTo: '/auth/login', pathMatch: 'full' },
    {
        path: "auth",
        loadChildren: "./auth/auth.module#AuthModule",
        canActivate: [UnAuthGuard],
        data: { userTypes: [UserType.none] }
    },
    {
        path: "customer",
        loadChildren: "./user/customer/customer.module#CustomerModule",
        canActivate: [AuthGuard],
        data: { userTypes: [UserType.customer] }
    },
    {
        path: "404",
        component: NotFoundComponent,
    },
    { path: '**', redirectTo: '/404' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }

