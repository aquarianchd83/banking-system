import { Injectable } from '@angular/core';
import swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor() { }

  info(message: string) {
    swal.fire(
      'Info!!',
      message,
      'success'
    );
  }

  warning(message: string): void {
    swal.fire(
      'Warning!!',
      message,
      'warning'
    );
  }

  error(message: string) {
    swal.fire(
      'Error!!',
      message,
      'error'
    );
  }

  success(moduleName: string, callback: any = null) {
    swal.fire(
      'Success!!',
      moduleName + ' has been saved successfully.',
      'success'
    );
    if (callback != null) {
      callback();
    }
  }
}
