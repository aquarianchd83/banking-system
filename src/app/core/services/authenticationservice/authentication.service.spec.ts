import { TestBed, inject } from '@angular/core/testing';

import { AuthenticationService } from './authentication.service';
import { SessionService } from '../sessionservice/session.service';
import { SessionServiceMock } from '../sessionservice/session.service.mock';

describe('AuthenticationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: SessionService, useClass: SessionServiceMock },
        AuthenticationService],

    });
  });

  it('should be created', inject([AuthenticationService], (service: AuthenticationService) => {
    expect(service).toBeTruthy();
  }));
});
