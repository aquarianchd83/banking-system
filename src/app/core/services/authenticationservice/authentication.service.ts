import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { environment } from 'src/environments/environment';
import { SessionService } from '../sessionservice/session.service';
import { Observable } from 'rxjs';
import { AuthenticatedUser } from './authenticated-user';
import { BaseService } from '../../helpers/base-service';
import { UserType } from '../../enum/user-type';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService extends BaseService {

  tokenVariableName = environment.sessionVariables.token;

  constructor(private sessionService: SessionService) {
    super();
  }

  getAuth(): AuthenticatedUser {
    var data: AuthenticatedUser = this.sessionService.get(this.tokenVariableName) as AuthenticatedUser;
    if (data != null) {
      return new AuthenticatedUser(data);
    }
    return null;
  }

  // Returns true if user is logged in
  get authenticated(): boolean {
    return this.getAuth() !== null;
  }



  public login(username: string, password: string): Observable<any> {

    return this.createObservable(() => {

      var users = environment.users;

      var user = users.filter(x => x.password == password && x.username == username)[0];

      if (user == null)
        throw "Invalid username && password ";

      var userInfo = new AuthenticatedUser({
        id: user.id,
        name: user.name,
        userType: UserType.customer,
        token: "bdjka7adas3jk"
      });

      this.sessionService.set(this.tokenVariableName, userInfo);

      return userInfo;
    });
  }

  logout() {
    localStorage.removeItem(this.tokenVariableName);
  }
}


