import { UserType } from "../../enum/user-type";

export class AuthenticatedUser {
  public id: number;
  public name: string;
  public token: string;
  public userType: UserType;

  public get isCustomer(): boolean {
    return this.userType == UserType.customer;
  }

  constructor(init: Partial<AuthenticatedUser>) {
    Object.assign(this, init);
  }
}
