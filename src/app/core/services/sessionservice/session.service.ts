import { Injectable, Inject } from '@angular/core';
import { LOCAL_STORAGE, WebStorageService } from 'angular-webstorage-service';

@Injectable()
export class SessionService {
  constructor(@Inject(LOCAL_STORAGE) private storage: WebStorageService) {
  }

  set(key, val): void {
    this.storage.set(key, val);
  }

  get(key): any {
    return this.storage.get(key);
  }
}
