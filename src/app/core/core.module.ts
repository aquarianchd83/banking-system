import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { StorageServiceModule } from 'angular-webstorage-service';

@NgModule({
  imports: [
    CommonModule,
    StorageServiceModule,
    [RouterModule.forChild([])]
  ],
  declarations: [],
  providers: [],
  exports: [RouterModule]
})
export class CoreModule { }
