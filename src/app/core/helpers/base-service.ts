import { ErrorHandler, Injectable } from '@angular/core';
import { AlertService } from '../services';
import { Observable } from 'rxjs';

@Injectable()
export class BaseService {

    public createObservable(callback: any): Observable<any> {
        return new Observable((observer) => {
            try {

                var response = callback();

                // observable execution
                observer.next(response)
                observer.complete()

            } catch (error) {
                observer.error(error);
            }
        });
    }

    public uniqueId(): number {
        return new Date().getUTCMilliseconds();
    }

}