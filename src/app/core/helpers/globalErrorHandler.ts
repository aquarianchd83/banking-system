import { ErrorHandler, Injectable } from '@angular/core';
import { AlertService } from '../services';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

    constructor(private alertService: AlertService) { }

    handleError(error) {
        console.log(error);
    }
}