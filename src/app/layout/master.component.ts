import { Component, OnInit, Input } from '@angular/core';
import { AuthenticationService } from 'src/app/core/services';
import { Router } from '@angular/router';
import { AuthenticatedUser } from 'src/app/core/services/authenticationservice/authenticated-user';

@Component({
  selector: 'app-admin',
  templateUrl: './master.component.html',
  styleUrls: ['./master.component.scss'],
})
export class AuthenticatedMasterComponent implements OnInit {

  public user: AuthenticatedUser = new AuthenticatedUser({});

  constructor(public authenticateService: AuthenticationService, private router: Router) {
    if (authenticateService.authenticated) {
      this.user = authenticateService.getAuth();
    }
  }

  ngOnInit() {

  }

  onLogout() {
    this.authenticateService.logout();
    this.router.navigateByUrl('/auth/login');
  }

}
