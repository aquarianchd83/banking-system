import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/share.module';
import { RouterModule } from '@angular/router';
import { AuthenticatedMasterComponent } from './master.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
  ],
  declarations: [
    AuthenticatedMasterComponent,
  ],
  providers: [
  ],
  exports: [
    AuthenticatedMasterComponent,
  ]

})

export class LayoutModule {

}
