import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaskNumber, MaskAlphabet } from './directives';


@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    MaskAlphabet,
    MaskNumber,
  ],
  exports: [
    MaskAlphabet,
    MaskNumber,
  ],
  providers: [

  ]
})
export class SharedModule {

}
