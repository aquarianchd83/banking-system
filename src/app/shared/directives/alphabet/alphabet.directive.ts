
import { Directive, ElementRef, HostListener, Input } from '@angular/core';



@Directive({
  selector: '[maskAlphabet]'
})
export class MaskAlphabet {

  @Input('maskAlphabet.min-length') min: number = 0;

  constructor(private el: ElementRef) {

  }


  @HostListener("keypress", ['$event'])
  onkeypress(e: any) {
    var charCode = e.keyCode;
    return ((charCode > 31 && charCode < 33) || (charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123)) && e.target.value.length < this.min;
  }
}
