import { Directive, HostListener, Input } from '@angular/core';


@Directive({
  selector: '[maskNumber]'
})
export class MaskNumber {

  @Input('maskNumber.min-length') min: number = 0;

  constructor() {

  }

  @HostListener("keypress", ['$event'])
  onKeyPress1(n: any) {
    var iKeyCode = n.keyCode;
    return !(iKeyCode = 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)) && n.target.value.length < this.min;
  }

}
