import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/share.module';
import { LayoutModule } from 'src/app/layout/layout.module';
import { TableModule } from 'primeng/table';
import { CustomerRoutingModule } from './customer-routing.module';
import { TransferFundComponent, CreateAccountComponent, ListAccountComponent } from './pages';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    CommonModule,
    CustomerRoutingModule,
    FormsModule,
    ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
    SharedModule,
    LayoutModule,
    TableModule,

  ],
  declarations: [
    TransferFundComponent,
    CreateAccountComponent,
    ListAccountComponent
  ],
  providers: [
  ],
  exports: [
  ]

})

export class CustomerModule {

}
