import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferFundComponent } from './transfer-fund.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from 'src/app/shared/share.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AccountService } from '../../services/account/account.service';
import { AlertService } from 'src/app/core/services';

describe('TransferFundComponent', () => {
  let component: TransferFundComponent;
  let fixture: ComponentFixture<TransferFundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TransferFundComponent],
      imports: [
        SharedModule,
        BrowserAnimationsModule, ,
        FormsModule,
        ReactiveFormsModule,
      ],
      providers: [AccountService, AlertService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferFundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
