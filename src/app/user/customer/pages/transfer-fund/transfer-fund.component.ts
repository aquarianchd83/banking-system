import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TransferAmountBindingModel } from '../../models/binding-models/transfer-amount';
import { AccountService } from '../../services/account/account.service';
import { AccountViewModel } from '../../models/view-models/account';
import { AlertService, AuthenticationService } from 'src/app/core/services';

@Component({
  selector: 'app-transfer-fund',
  templateUrl: './transfer-fund.component.html',
  styleUrls: ['./transfer-fund.component.css']
})
export class TransferFundComponent implements OnInit {

  public model: TransferAmountBindingModel = new TransferAmountBindingModel({});
  transferForm: FormGroup;
  submitted: boolean = false;
  accountFroms: Array<AccountViewModel> = [];
  accountTos: Array<AccountViewModel> = [];

  constructor(private accountService: AccountService, private authenticatedService: AuthenticationService, private alertService: AlertService) {

    var loginUserId = this.authenticatedService.getAuth().id;

    this.accountService.getOwnAccounts(loginUserId).subscribe((accounts: Array<AccountViewModel>) => {
      this.accountFroms = accounts;
    });

    this.accountService.getOtherCustomerAccounts(loginUserId).subscribe((accounts: Array<AccountViewModel>) => {
      this.accountTos = accounts;
    });
  }

  ngOnInit() {
    this.setFormConfiguration();
  }


  setFormConfiguration() {
    this.transferForm = new FormGroup({
      accountFromId: new FormControl(this.model.accountFromId, Validators.required),
      accountToId: new FormControl(this.model.accountToId, Validators.required),
      amount: new FormControl(this.model.amount, Validators.required),
    });
  }

  get f() { return this.transferForm.controls; }

  onAmountTransfer() {
    this.submitted = true;
    if (this.transferForm.valid) {
      this.accountService.transferAmount(this.transferForm.value).subscribe(() => {
        this.alertService.success("Amount transferred", () => {
          this.submitted = false;
          this.model = new TransferAmountBindingModel({});
          this.setFormConfiguration();
        });
      });
    }
  }
}
