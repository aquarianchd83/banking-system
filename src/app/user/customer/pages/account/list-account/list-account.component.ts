import { Component, OnInit } from '@angular/core';
import { AccountService } from '../../../services/account/account.service';
import { AuthenticationService } from 'src/app/core/services';

@Component({
  selector: 'app-list-account',
  templateUrl: './list-account.component.html',
  styleUrls: ['./list-account.component.css']
})
export class ListAccountComponent implements OnInit {

  cols: any[];
  accounts: any = [];

  constructor(private accountService: AccountService, private authenticatedService: AuthenticationService) { }

  ngOnInit() {

    this.cols = [
      { field: 'holderName', header: 'Holder Name' },
      { field: 'accountNumber', header: 'Account No.' },
      { field: 'bankName', header: 'Bank Name' },
      { field: 'type', header: 'Type' },
      { field: 'isOwnAccount', header: 'Own Account' },
      { field: 'balance', header: 'Balance' },
    ];

    this.onRenderAccounts();
  }

  onRenderAccounts() {
    this.accountService.getOwnAccounts(this.authenticatedService.getAuth().id).subscribe((values) => {
      this.accounts = values;
    });
  }

}
