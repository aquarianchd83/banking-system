import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAccountComponent } from './list-account.component';
import { TableModule } from 'primeng/table';
import { AccountService } from '../../../services/account/account.service';
import { SessionService } from 'src/app/core/services';
import { SessionServiceMock } from 'src/app/core/services/sessionservice/session.service.mock';

describe('ListAccountComponent', () => {
  let component: ListAccountComponent;
  let fixture: ComponentFixture<ListAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListAccountComponent],
      imports: [TableModule],
      providers: [
        AccountService,
        { provide: SessionService, useClass: SessionServiceMock },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
