import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertService, AuthenticationService } from 'src/app/core/services';
import { AccountService } from '../../../services/account/account.service';
import { AccountBindingModel } from '../../../models/binding-models/account';

@Component({
  selector: 'app-uc-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.css']
})
export class CreateAccountComponent implements OnInit {

  constructor(private accountService: AccountService, private authenticatedService: AuthenticationService, private alertService: AlertService) { }

  bankNames: any = ['PNB', 'SBI', 'YES BANK', 'HDFC', 'ICICI'];
  public model: AccountBindingModel = new AccountBindingModel({});
  createAccountForm: FormGroup;
  submitted: boolean = false;

  ngOnInit() {
    this.setFormConfiguration();
  }

  setFormConfiguration() {
    this.createAccountForm = new FormGroup({
      id: new FormControl(this.model.id),
      holderName: new FormControl(this.model.holderName, Validators.required),
      type: new FormControl(this.model.type, Validators.required),
      bankName: new FormControl(this.model.bankName, Validators.required),
      accountNumber: new FormControl(this.model.accountNumber, Validators.required),
      isOwnAccount: new FormControl(false),
      balance: new FormControl(0),
      createdBy: new FormControl(this.authenticatedService.getAuth().id),
    });
  }

  get f() { return this.createAccountForm.controls; }

  onSave() {
    this.submitted = true;
    if (this.createAccountForm.valid) {
      this.accountService.add(this.createAccountForm.value).subscribe(() => {
        this.alertService.success("Account", () => {
          this.submitted = false;
          this.model = new AccountBindingModel({});
          this.setFormConfiguration();
        });
      });
    }
  }
}