import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAccountComponent } from './create-account.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/share.module';
import { AccountService } from '../../../services/account/account.service';
import { SessionService } from 'src/app/core/services';
import { SessionServiceMock } from 'src/app/core/services/sessionservice/session.service.mock';

describe('CreateAccountComponent', () => {
  let component: CreateAccountComponent;
  let fixture: ComponentFixture<CreateAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateAccountComponent],
      imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        SharedModule,
      ],
      providers: [
        AccountService,
        { provide: SessionService, useClass: SessionServiceMock }
      ]
    })
      .compileComponents().then(() => {
        fixture = TestBed.createComponent(CreateAccountComponent);
        component = fixture.componentInstance;
      });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set submitted to false while loading the page', async(() => {
    expect(component.submitted).toBeFalsy();
  }));

  it('should set submitted to true while submitting the form', async(() => {
    component.onSave();
    expect(component.submitted).toBeTruthy();
  }));

  it('try with invalid form', async(() => {
    component.createAccountForm.controls['holderName'].setValue('');
    component.createAccountForm.controls['type'].setValue('');
    component.createAccountForm.controls['bankName'].setValue('');
    component.createAccountForm.controls['accountNumber'].setValue('');
    expect(component.createAccountForm.valid).toBeFalsy();
  }));

  it('try with valid form', async(() => {
    component.createAccountForm.controls['holderName'].setValue('Harish');
    component.createAccountForm.controls['type'].setValue('current');
    component.createAccountForm.controls['bankName'].setValue('SBI');
    component.createAccountForm.controls['accountNumber'].setValue('3874283648638');
    expect(component.createAccountForm.valid).toBeTruthy();
  }));
});
