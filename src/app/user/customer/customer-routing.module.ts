import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TransferFundComponent, ListAccountComponent, CreateAccountComponent } from './pages';
import { AuthenticatedMasterComponent } from 'src/app/layout/master.component';

const routes: Routes = [
    {
        path: "",
        component: AuthenticatedMasterComponent,
        children: [
            { path: '', component: ListAccountComponent },

            // account Componets
            { path: 'account-list', component: ListAccountComponent },
            { path: 'account-create', component: CreateAccountComponent },
            { path: 'transfer-fund', component: TransferFundComponent },

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CustomerRoutingModule { }

