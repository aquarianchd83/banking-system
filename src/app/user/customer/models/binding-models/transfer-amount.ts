export class TransferAmountBindingModel {
    accountFromId: string;
    accountToId: string;
    amount: number;
    constructor(init: Partial<TransferAmountBindingModel>) {
        Object.assign(this, init);
    }
}
