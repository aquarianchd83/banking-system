export class AccountBindingModel {
  id: number;
  holderName: string;
  type: string;
  bankName: string;
  accountNumber: number;
  isOwnAccount: boolean;
  balance: number;
  createdById:number;
  constructor(init: Partial<AccountBindingModel>) {
    Object.assign(this, init);
  }
}
