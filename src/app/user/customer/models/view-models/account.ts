export class AccountViewModel {
    id: number;
    holderName: string;
    constructor(init: Partial<AccountViewModel>) {
        Object.assign(this, init);
    }
}
