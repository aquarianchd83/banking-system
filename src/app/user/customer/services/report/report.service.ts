import { Injectable } from '@angular/core';
import { BaseService } from 'src/app/core/helpers/base-service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReportService extends BaseService {

  constructor() {
    super();
  }

  getLedgerReport(): Observable<any> {
    return this.createObservable(() => {
      return [
        {
          holderName: "Harish",
          credit: "1,000",
          debit: "0",
          closingBalance: "1,000"
        },
        {
          holderName: "Harish",
          credit: "0",
          debit: "1,000",
          closingBalance: "0"
        }
      ];
    });
  }
}
