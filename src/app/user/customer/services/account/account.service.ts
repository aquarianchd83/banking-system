import { Injectable } from '@angular/core';
import { AccountBindingModel } from '../../models/binding-models/account';
import { SessionService } from 'src/app/core/services';
import { BaseService } from 'src/app/core/helpers/base-service';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TransferAmountBindingModel } from '../../models/binding-models/transfer-amount';
import 'rxjs/add/operator/map';
@Injectable({
  providedIn: 'root'
})

export class AccountService extends BaseService {

  constructor(private sessionService: SessionService) {
    super();
  }

  add(data: AccountBindingModel): Observable<any> {
    return this.createObservable(() => {
      this.getAll().subscribe((accounts: Array<any>) => {
        data.id = this.uniqueId();
        accounts.push(data);
        this.sessionService.set(environment.sessionVariables.accounts, accounts);
      });
    });
  }

  getAll(): Observable<Array<AccountBindingModel>> {
    return this.createObservable(() => {

      var accounts = this.sessionService.get(environment.sessionVariables.accounts);

      if (accounts == null)
        accounts = [];

      return accounts;
    });
  }



  getOwnAccounts(id: number): Observable<Array<AccountBindingModel>> {
    return this.createObservable(() => {

      var accounts = this.sessionService.get(environment.sessionVariables.accounts);

      var accounts = accounts.filter(x => x.createdBy == id);

      if (accounts == null)
        accounts = [];

      return accounts;
    });
  }

  getOtherCustomerAccounts(id: number): Observable<Array<AccountBindingModel>> {
    return this.createObservable(() => {

      var accounts = this.sessionService.get(environment.sessionVariables.accounts);

      var accounts = accounts.filter(x => x.createdBy != id);

      if (accounts == null)
        accounts = [];

      return accounts;
    });
  }

  transferAmount(data: TransferAmountBindingModel) {
    return this.createObservable(() => {
      console.log(data);

      var accounts = this.sessionService.get(environment.sessionVariables.accounts);

      var fromBalance = accounts.filter(x => x.id == data.accountFromId)[0].balance;
      var toBalance = accounts.filter(x => x.id == data.accountToId)[0].balance;


      var closingAmoutAfterDebit = parseFloat(fromBalance) - data.amount;
      var closingAmoutAfterCredit = parseFloat(toBalance) + parseFloat(data.amount.toString());
      debugger;

      accounts.filter(x => x.id == data.accountFromId)[0].balance = closingAmoutAfterDebit;
      accounts.filter(x => x.id == data.accountToId)[0].balance = closingAmoutAfterCredit;


      this.sessionService.set(environment.sessionVariables.accounts, accounts);
    });
  }
}

