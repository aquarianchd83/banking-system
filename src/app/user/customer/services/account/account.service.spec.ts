import { TestBed, inject } from '@angular/core/testing';

import { AccountService } from './account.service';
import { SessionService } from 'src/app/core/services';
import { SessionServiceMock } from 'src/app/core/services/sessionservice/session.service.mock';

describe('AccountService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccountService, { provide: SessionService, useClass: SessionServiceMock }],

    });
  });

  it('should be created', inject([AccountService], (service: AccountService) => {
    expect(service).toBeTruthy();
  }));
});
